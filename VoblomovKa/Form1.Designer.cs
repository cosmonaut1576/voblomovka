﻿namespace VoblomovKa
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ListOfFriends = new System.Windows.Forms.ListBox();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.buttonSendMessage = new System.Windows.Forms.Button();
            this.buttonSendPost = new System.Windows.Forms.Button();
            this.buttonLikes = new System.Windows.Forms.Button();
            this.buttonCheckBirth = new System.Windows.Forms.Button();
            this.buttonSendCongrat = new System.Windows.Forms.Button();
            this.BirthdayGuys = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.MediumAquamarine;
            this.label1.Location = new System.Drawing.Point(219, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "VoblomovKa";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(45, 335);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Auth";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.ButtonAuth_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(433, 335);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // ListOfFriends
            // 
            this.ListOfFriends.BackColor = System.Drawing.Color.Gainsboro;
            this.ListOfFriends.FormattingEnabled = true;
            this.ListOfFriends.Location = new System.Drawing.Point(6, 76);
            this.ListOfFriends.Name = "ListOfFriends";
            this.ListOfFriends.Size = new System.Drawing.Size(228, 108);
            this.ListOfFriends.TabIndex = 3;
            this.ListOfFriends.SelectedIndexChanged += new System.EventHandler(this.ListOfFriends_SelectedIndexChanged);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.BackColor = System.Drawing.Color.Gainsboro;
            this.textBoxMessage.Location = new System.Drawing.Point(12, 248);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBoxMessage.Size = new System.Drawing.Size(222, 40);
            this.textBoxMessage.TabIndex = 4;
            // 
            // buttonSendMessage
            // 
            this.buttonSendMessage.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSendMessage.Location = new System.Drawing.Point(12, 294);
            this.buttonSendMessage.Name = "buttonSendMessage";
            this.buttonSendMessage.Size = new System.Drawing.Size(95, 23);
            this.buttonSendMessage.TabIndex = 5;
            this.buttonSendMessage.Text = "Send to friend";
            this.buttonSendMessage.UseVisualStyleBackColor = true;
            this.buttonSendMessage.Click += new System.EventHandler(this.ButtonSendMessage_Click);
            // 
            // buttonSendPost
            // 
            this.buttonSendPost.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSendPost.Location = new System.Drawing.Point(126, 294);
            this.buttonSendPost.Name = "buttonSendPost";
            this.buttonSendPost.Size = new System.Drawing.Size(108, 23);
            this.buttonSendPost.TabIndex = 6;
            this.buttonSendPost.Text = "Send to the wall";
            this.buttonSendPost.UseVisualStyleBackColor = true;
            this.buttonSendPost.Click += new System.EventHandler(this.ButtonSendPost_Click);
            // 
            // buttonLikes
            // 
            this.buttonLikes.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLikes.Location = new System.Drawing.Point(12, 192);
            this.buttonLikes.Name = "buttonLikes";
            this.buttonLikes.Size = new System.Drawing.Size(196, 23);
            this.buttonLikes.TabIndex = 7;
            this.buttonLikes.Text = "Put <3 to all posts on the wall";
            this.buttonLikes.UseVisualStyleBackColor = true;
            this.buttonLikes.Click += new System.EventHandler(this.buttonLike_Click);
            // 
            // buttonCheckBirth
            // 
            this.buttonCheckBirth.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCheckBirth.Location = new System.Drawing.Point(304, 199);
            this.buttonCheckBirth.Name = "buttonCheckBirth";
            this.buttonCheckBirth.Size = new System.Drawing.Size(172, 23);
            this.buttonCheckBirth.TabIndex = 8;
            this.buttonCheckBirth.Text = "Check friends on birthday";
            this.buttonCheckBirth.UseVisualStyleBackColor = true;
            this.buttonCheckBirth.Click += new System.EventHandler(this.buttonCheckBirth_Click);
            // 
            // buttonSendCongrat
            // 
            this.buttonSendCongrat.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSendCongrat.Location = new System.Drawing.Point(296, 232);
            this.buttonSendCongrat.Name = "buttonSendCongrat";
            this.buttonSendCongrat.Size = new System.Drawing.Size(189, 23);
            this.buttonSendCongrat.TabIndex = 9;
            this.buttonSendCongrat.Text = "Send \"Happy Birthday\" message";
            this.buttonSendCongrat.UseVisualStyleBackColor = true;
            this.buttonSendCongrat.Click += new System.EventHandler(this.buttonSendCongrat_Click);
            // 
            // BirthdayGuys
            // 
            this.BirthdayGuys.BackColor = System.Drawing.Color.Gainsboro;
            this.BirthdayGuys.FormattingEnabled = true;
            this.BirthdayGuys.Location = new System.Drawing.Point(269, 76);
            this.BirthdayGuys.Name = "BirthdayGuys";
            this.BirthdayGuys.Size = new System.Drawing.Size(228, 108);
            this.BirthdayGuys.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 389);
            this.Controls.Add(this.BirthdayGuys);
            this.Controls.Add(this.buttonSendCongrat);
            this.Controls.Add(this.buttonCheckBirth);
            this.Controls.Add(this.buttonLikes);
            this.Controls.Add(this.buttonSendPost);
            this.Controls.Add(this.buttonSendMessage);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.ListOfFriends);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox ListOfFriends;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Button buttonSendMessage;
        private System.Windows.Forms.Button buttonSendPost;
        private System.Windows.Forms.Button buttonLikes;
        private System.Windows.Forms.Button buttonCheckBirth;
        private System.Windows.Forms.Button buttonSendCongrat;
        private System.Windows.Forms.ListBox BirthdayGuys;
    }
}

