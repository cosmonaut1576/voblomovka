﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.RequestParams;
using static System.Math;

namespace VoblomovKa
{
    public partial class Form1 : Form
        {
            VkApi vkAPI = new VkApi();
        long? thisUserID = 0;
        long ChosenUserID = 0;
            public Form1()
            {
                InitializeComponent();
            }

            private void Form1_Load(object sender, EventArgs e)
            {

            }

        private void ButtonAuth_Click(object sender, EventArgs e)
        {
            ulong appID = 5985068;
            string login = "irishka.bereg97@mail.ru";
            string password = File.ReadAllText(@"D:\password.txt");
            Settings settings = Settings.All;
            try {
                vkAPI.Authorize(new ApiAuthParams
                {
                    ApplicationId = Convert.ToUInt64(appID),
                    Login = login,
                    Password = password,
                    Settings = settings
                });
                thisUserID = vkAPI.UserId;
                GetFriends();
            }
            catch {
                MessageBox.Show("Не удалось авторизоваться.");
            }
            //string scope = "friends";
            //string url = "https://oauth.vk.com/authorize?client_id=" + appID + "&display=popup&redirect_uri=https://oauth.vk.com/blank.html&scope=" + scope + "&response_type=token";
        }

        public void GetFriends() {
            DateTime dateNow = DateTime.Today;
            var users = vkAPI.Friends.Get(new FriendsGetParams
            {
                UserId = long.Parse(thisUserID.ToString()),
                Fields = ProfileFields.FirstName | ProfileFields.LastName | ProfileFields.Uid
            });
            foreach (var friend in users)
            {
               ListOfFriends.Items.Add(friend.FirstName + " " + friend.LastName + " " + friend.Id);
            }
        }

        

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonSendMessage_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text != "") {
                vkAPI.Messages.Send(new MessagesSendParams{
                    UserId = ChosenUserID,
                    Message = textBoxMessage.Text
                });
                textBoxMessage.Text = "";
            }
        }

        private void ButtonSendPost_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text != "")
            {
                vkAPI.Wall.Post(new WallPostParams
                {
                    OwnerId = ChosenUserID,
                    FriendsOnly = true,
                    Message = textBoxMessage.Text
                });
                textBoxMessage.Text = "";
            }
        }

        private void buttonLike_Click(object sender, EventArgs e)
        {
            var posts = vkAPI.Wall.Get(new WallGetParams
            {
                OwnerId = ChosenUserID
            });
            foreach (var post in posts.WallPosts)
            {
                vkAPI.Likes.Add(new LikesAddParams
                {
                    ItemId = (long)post.Id,
                    Type = LikeObjectType.Post,
                    OwnerId = ChosenUserID
                });
            }
        }

        private void buttonCheckBirth_Click(object sender, EventArgs e)
        {
            var users = vkAPI.Friends.Get(new FriendsGetParams
            {
                UserId = long.Parse(thisUserID.ToString()),
                Fields = ProfileFields.FirstName | ProfileFields.LastName | ProfileFields.BirthDate
            });
            foreach (var friend in users)
            {
                try
                {
                    DateTime dateNow = DateTime.Today;
                    string[] parseDateBirth;
                    parseDateBirth = friend.BirthDate.Split(new char[] { '.' }, 3);
                    int difference = Math.Abs(dateNow.Day - Convert.ToInt32(parseDateBirth[0])) + Math.Abs(dateNow.Month - Convert.ToInt32(parseDateBirth[1]));
                    if (difference == 0)
                    {
                        ListOfFriends.Items.Add(friend.FirstName + " " + friend.LastName + " ");
                    }
                }
                catch {
                        MessageBox.Show("На сегодня поздравлений не намечается.");
                }
            }
        }

        private void buttonSendCongrat_Click(object sender, EventArgs e)
        {
            var users = vkAPI.Friends.Get(new FriendsGetParams
            {
                UserId = long.Parse(thisUserID.ToString()),
                Fields = ProfileFields.FirstName | ProfileFields.LastName | ProfileFields.BirthDate
            });
            foreach (var friend in users)
            {
                try
                {
                    DateTime dateNow = DateTime.Today;
                    string[] parseDateBirth;
                    parseDateBirth = friend.BirthDate.Split(new char[] { '.' }, 3);
                    int difference = Math.Abs(dateNow.Day - Convert.ToInt32(parseDateBirth[0])) + Math.Abs(dateNow.Month - Convert.ToInt32(parseDateBirth[1]));
                    if (difference == 0)
                    {
                        vkAPI.Messages.Send(new MessagesSendParams
                        {
                            UserId = friend.Id,
                            Message = "С Днём рождения!!!"
                        });
                    }
                }
                catch
                {
                    MessageBox.Show("На сегодня поздравлений не намечается.");
                }
                
            }
        }

        private void ListOfFriends_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChosenUserID = long.Parse(ListOfFriends.Text.Substring(ListOfFriends.Text.LastIndexOf(' ')));
        }
    }
}
